//
//  RestaurantLogin.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/14/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//
import Foundation
//model class for Restaurant Login Information
class RestaurantLogin:NSObject{
    var emailId:String //variale of type String to store emailid
    var password:String //variale of type String to store passsword
    var entityId: String? //variale of type String to store entityid
    
   //Initializes  the attributes of this class with the values passed
    init(emailId:String, password:String) {
        self.emailId = emailId
        self.password = password
    }
    //Function to map variables in this class to the store created in kinvey
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId,
            "emailId" : "emailId",
            "password" : "password",
        ]
    }
}

