//
//  RegisterViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/13/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit
//Viewcontroller to register a new user to the restaurants
class RegisterViewController: UIViewController ,Operation{
    @IBOutlet weak var RestaurantNameTF: UITextField! //Textfield to store the restaurant name
    
    @IBOutlet weak var EmailIdTF: UITextField! //Textfield to store emailid
    @IBOutlet weak var PasswordTF: UITextField! //Textfield to store password
    @IBOutlet weak var addressTF: UITextField! //Textfield to store address
    @IBOutlet weak var cityTF: UITextField! //Textfield to store city input
    @IBOutlet weak var confirmPasswordTF: UITextField! //Textfield to store password
    @IBOutlet weak var zipTF: UITextField! //Textfield to store zip code
    @IBOutlet weak var stateTF: UITextField! //Textfield to store state
    @IBOutlet weak var countryTF: UITextField! //Textfield to store country
    @IBOutlet weak var phoneNumberTF: UITextField! //Textfield to store phone number given
    var lat:CLLocationDegrees = 0.0 //variable of type CLLocationDegrees to store latitude
    var long:CLLocationDegrees = 0.0 //variable of type CLLocationDegrees to store longitude
    var kinveyClientOperation:KinveyOperation! //Reference to KinveyOperation class
    var  restaurant:Restaurant! //Reference to Restaurant class
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Registration" //Sets the title for the navigation item
        kinveyClientOperation = KinveyOperation(operation:self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //Function to validate the email address given
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(testStr)
        return result
    }
    //Function to validate registration and display the alert messages
    @IBAction func Register(sender: AnyObject) {
        if (RestaurantNameTF.text!.isEmpty || EmailIdTF.text!.isEmpty || PasswordTF.text!.isEmpty){
            self.displayAlertControllerWithTitle("Registration Failed", message: "All fields are Required")
        }
        else if  PasswordTF.text != confirmPasswordTF.text! {
            self.displayAlertControllerWithTitle("Mismatch", message: "password doesnot match")
        }
        else if (!isValidEmail(EmailIdTF.text!))
        {
            self.displayAlertControllerWithTitle("Invalid Email", message: "insert proper email address")
        }
        else{
            self.geo()
        }
    }
    //Function to convert given address into latitude and longitude
    func geo(){
        let geoCoder = CLGeocoder()
        let addressString = "\(addressTF.text) \(cityTF.text) \(stateTF.text) \(zipTF.text)"
        geoCoder.geocodeAddressString(addressString, completionHandler:
            {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
                if error != nil {
                    print("Geocode failed with error: \(error!.localizedDescription)")
                } else if placemarks!.count > 0 {
                    let placemark = placemarks![0] as! CLPlacemark
                    let location = placemark.location
                    let coords = location!.coordinate
                    self.lat = (location?.coordinate.latitude)!
                    self.long = (location?.coordinate.longitude)!
                    //Creates a restaurant object with all attributes passed
                    self.restaurant = Restaurant(restaurantName: self.RestaurantNameTF.text!, emailId: self.EmailIdTF.text!, password: self.PasswordTF.text!, address:self.addressTF.text!, city: self.cityTF.text!, state: self.stateTF.text!, country: self.countryTF.text!, phoneNumber: self.phoneNumberTF.text!,lat: self.lat,long: self.long,zipCode: self.zipTF.text!)
                    self.kinveyClientOperation.registration(self.restaurant) //Calls the kinvey class function named register and passes this restaurant object
                }
        })
    }
    //Function to display the alert message on successful registration
    func SignupSuccess() {
        self.displayAlertControllerWithSuccess("Registration successful", message: "Welcome!")
    }
    //Function to display the alert message on failed registration
    func Signupfailed() {
        
        self.displayAlertControllerWithTitle("Registration failed", message: "Try Again")
    }
   //Function to display alert messages and navigate to LoginViewController if the login is successful
    func displayAlertControllerWithSuccess(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("registerSuccess", sender: self)}))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    //Function to display alert messages
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
}
