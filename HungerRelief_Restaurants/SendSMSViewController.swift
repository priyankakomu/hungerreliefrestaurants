//
//  SendSMSViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Priyanka on 4/16/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//


import UIKit
import MessageUI
//class to send messages to nearest ngo locations
class SendSMSViewController: UIViewController,MFMessageComposeViewControllerDelegate{
    //variable of String to store phone number
    var phoneNumber:String!
    //Label to store phone number
    @IBOutlet weak var phoneNumberTF: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTF.text = phoneNumber // Assign the phone number retreived from Ngosstore in map view controller
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //function to send messages to the given recipient
    @IBAction func sendSMS(sender: AnyObject) {
        let messageVC = MFMessageComposeViewController()
        messageVC.body = "Items are ready to pick";
        let send = String(UTF8String: phoneNumberTF.text!)
        messageVC.recipients = [send!]
        messageVC.messageComposeDelegate = self;
        self.presentViewController(messageVC, animated: false, completion: nil)
    }
    //function to tell the delegate that the user finished composing the message.
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        switch (result.rawValue) {
        case MessageComposeResultCancelled.rawValue:self.dismissViewControllerAnimated(true, completion: nil)
        case MessageComposeResultFailed.rawValue:self.dismissViewControllerAnimated(true, completion: nil)
        case MessageComposeResultSent.rawValue:self.dismissViewControllerAnimated(true, completion: nil)
        default:
            break;
        }
    }
    
}


