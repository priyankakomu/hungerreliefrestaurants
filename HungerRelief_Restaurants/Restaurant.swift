//
//  Restaurant.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/13/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import Foundation
//model class to store Restaurant Information
class Restaurant:NSObject{
    var restaurantName:String //variable of type string to store restaurant name
    var emailId:String //variable of type string to store emailid
    var password:String //variable of type string to store password
    var address:String //variable of type string to store address
    var city:String //variable of type string to store city
    var state:String //variable of type string to store state
    var country:String //variable of type string to store country
    var phoneNumber:String //variable of type string to store phone number
    var lat:CLLocationDegrees //variable of type CLLocationDegrees to store latitude
    var long:CLLocationDegrees //variable of type CLLocationDegrees to store longitude
    var zipCode:String //variable of type string to store zipcode
    var entityId: String? //variable of type string to store entityid
    override init(){
        self.restaurantName = ""
        self.emailId = ""
        self.password = ""
        self.address = ""
        self.city = ""
        self.state = ""
        self.country = ""
        self.phoneNumber = ""
        self.lat = 0.0
        self.long = 0.0
        self.zipCode = ""
    }
  //Initializes  the attributes of this class with the values passed
    init(restaurantName:String,emailId:String,password:String,address:String,city:String,state:String,country:String,phoneNumber:String,lat:CLLocationDegrees,long:CLLocationDegrees,zipCode:String){
        self.restaurantName = restaurantName
        self.emailId = emailId
        self.password = password
        self.address = address
        self.city = city
        self.state = state
        self.country = country
        self.phoneNumber = phoneNumber
        self.lat = lat
        self.long = long
        self.zipCode = zipCode
    }
   //Function to map variables in this class to the store created in kinvey
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId,
            "restaurantName" : "restaurantName",
            "emailId" : "emailId",
            "password" : "password",
            "address":"address",
            "city":"city",
            "state":"state",
            "country":"country",
            "phoneNumber":"phoneNumber",
            "lat":"lat",
            "long":"long",
            "zipCode" : "zipCode"
        ]
    }
    
}