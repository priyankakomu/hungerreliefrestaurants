//
//  Ngos.swift
//  HungerRelief_Restaurants
//
//  Created by Valleshetti,Anwesh on 4/14/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import Foundation
// model class to store  Ngos information
class Ngos:NSObject{
    var ngosName:String //variable to store name of Ngos
    var emailId:String //variable of type string to store emailid
    var password:String //variable to store password
    var address:String //variabel of type String to store address
    var city:String //Function to map variables in this class to the store created in kinvey
    var state:String //variale of type String to store state
    var country:String //variale of type String to store country
    var phoneNumber:String //variale of type String to store phoneNumber
    var latitude:CLLocationDegrees //variale f type CLLocationDegrees to store latitude
    var longitude:CLLocationDegrees //variale of type CLLocationDegrees to store longitude
    var entityId: String? //variale of type String to store entityId
    override init(){
        self.ngosName = ""
        self.emailId = ""
        self.password = ""
        self.address = ""
        self.city = ""
        self.state = ""
        self.country = ""
        self.phoneNumber = ""
        self.latitude = 0.0
        self.longitude = 0.0
    }
    //Initializes  the attributes of this class with the values passed
    init(ngosName:String,emailId:String,password:String,address:String,city:String,state:String,country:String,phoneNumber:String,latitude:CLLocationDegrees,longitude:CLLocationDegrees){
        self.ngosName = ngosName
        self.emailId = emailId
        self.password = password
        self.address = address
        self.city = city
        self.state = state
        self.country = country
        self.phoneNumber = phoneNumber
        self.latitude = latitude
        self.longitude = longitude
    }
    //Function to map variables in this class to the store created in kinvey
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId,
            "ngosName" : "ngosName",
            "emailId" : "emailId",
            "password" : "password",
            "address":"address",
            "city":"city",
            "state":"state",
            "country":"country",
            "phoneNumber":"phoneNumber",
            "latitude":"latitude",
            "longitude":"longitude"
            
            
        ]
    }
    
}
