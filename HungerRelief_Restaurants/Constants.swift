//
//  Constants.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 4/6/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import Foundation
//Class to declare constants to use them globally
class Constants{
    static let USERNAME = "emailId"
    static let lat = "latitude"
    static let long = "longitude"
}