//
//  KinveyOperation.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/13/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import Foundation

@objc protocol Operation{
    //this function is invoked for Successful login
    optional func loginSuccess()
    //this function is invoked if  login is not successful
    optional func loginFailed()
    //this function is called  if signUp is Successful
    optional func SignupSuccess()
     //this function is called  if signUp is failed
    optional func Signupfailed()
     //this function is called to fetch Items
    optional func fetch(item:[Item])
     //this function is called  to fetch Location
    optional func fetchLocation(restaurant:Restaurant)
    //this function is called to fetch Restaurants
    optional func fetchNgoRecord(ngo:[Ngos])
    
}
// class to perform backend operations like storing and retrieving from kinvey database
class KinveyOperation{
    var restaurant:Restaurant!//this create a reference to Class  Restaurant
    var restaurantlogin:RestaurantLogin! //this create a reference to Class RestaurantLogin
    let restaurantStore:KCSAppdataStore! // this create a reference to restaurantStore In Kinvey
    let itemsStore:KCSAppdataStore! // this create a reference to itemsStore In Kinvey
    let ngosStore:KCSAppdataStore! //this create a reference to NgoStore In Kinvey
    let operationDelegate:Operation! // this is to create a reference to protocol operation
   
    let defaults = NSUserDefaults.standardUserDefaults()
     // This constructor Initializes all stores by creating collections for them and assigning delegation property to operation protocol
    init(operation:Operation){
        restaurantStore = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Restaurant",
            KCSStoreKeyCollectionTemplateClass : Restaurant.self
            ])
       itemsStore = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Item",
            KCSStoreKeyCollectionTemplateClass : Item.self
            ]) // This collection is 
        ngosStore = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Ngos",
            KCSStoreKeyCollectionTemplateClass : Ngos.self
            ])
        self.operationDelegate = operation
    }
     //  This function is checks the passed ngo is present in KCSUser if it not present then login failed  function of LoginViewController is called else loginSuccess method is called
    func login(restaurantlogin:RestaurantLogin){
        KCSUser.loginWithUsername(
            restaurantlogin.emailId,
            password: restaurantlogin.password,
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operationDelegate.loginSuccess!()
                    self.defaults.setValue(restaurantlogin.emailId, forKey: Constants.USERNAME)
                    
                    self.defaults.synchronize()
                    
                } else {
                    
                    self.operationDelegate.loginFailed!()
                }
            }
        )
    }
     //  This function is checks the passed ngo is present in KCSUser if it not present then signup failed  function of RegisterViewController is called else that  ngo is registerted into the KCSUser and saved in the ngosStore
    func registration(let restaurant:Restaurant){
        
        KCSUser.userWithUsername(
            restaurant.emailId,
            password:restaurant.password ,
            fieldsAndValues: [
                KCSUserAttributeGivenname : restaurant.restaurantName,
            ],
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operationDelegate.SignupSuccess!()
                    self.restaurantStore.saveObject(
                        restaurant,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                            } else {
                                print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                            }
                        },
                        withProgressBlock: nil
                    )
                } else {
                    self.operationDelegate.Signupfailed!()
                }
            }
        )
    }
     // This functions is used to save the items in the itemsStore
    func saveItem(let item:Item){
        
        self.itemsStore.saveObject(
            item,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                } else {
                    print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                }
            },
            withProgressBlock: nil
        )
    }
       // This function is used to fetch the Coordinates from restaurantStore which exactly matches with the given emailId
    func fetchCoordinates(emailId:String){
        let query = KCSQuery(onField: "emailId", withExactMatchForValue: emailId)
        restaurantStore.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil == nil {
                    if let objects = objectsOrNil as [AnyObject]!{
                        for object in objects{
                            let item = object as! Restaurant
                            self.operationDelegate.fetchLocation!(item)
                        }
                    }
                }
                else {
                    
                    print("Error")
                }
            },
            withProgressBlock: nil
        )
    }
      // This function is used to fetch the ngos from restaurantStore
    func fetchNgo(){
        
        ngosStore.queryWithQuery(
            KCSQuery(),
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil == nil {
                    
                    let objects = objectsOrNil as! [Ngos]
                    
                    self.operationDelegate.fetchNgoRecord!(objects)
                }
                else {
                    
                    print("Error")
                }
            },
            withProgressBlock: nil
        )
    }
    // This function is used to fetch the items from itemsStore
    func fetchItem() {
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "emailId", withExactMatchForValue: userValue)
        itemsStore.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil == nil {
                    
                    let objects = objectsOrNil as! [Item]
                    
                    self.operationDelegate.fetch!(objects)
                } else {
                    
                    print("Error")
                }
            },
            withProgressBlock: nil
        )
    }
}

