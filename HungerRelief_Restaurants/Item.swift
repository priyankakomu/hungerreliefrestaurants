//
//  Item.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/21/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import Foundation
// model class  to store food item information
class Item:NSObject{
    var itemName:String //variable of type String to store name of item
    var type:String //variable of type String to store type of item
    var quantity:String //variable of type String to store quantity of item
    var emailId:String //variable of type String to emailid
    var entityId: String? //variable of type String to entityid
    var flag:Bool = false //variable of type boolean to store flag value
    //Initializes the variables
    override init(){
        self.itemName = ""
        self.type = ""
        self.quantity = ""
        self.emailId = ""
    }
    //Initializes  the attributes of this class with the values passed
    init(itemName:String,type:String,quantity:String,emailId:String){
        self.itemName = itemName
        self.type = type
        self.quantity = quantity
        self.emailId = emailId
    }
    //Function to map variables in this class to the store created in kinvey
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId,
            "itemName" : "itemName",
            "type" : "type",
            "quantity" : "quantity",
            "emailId" :"emailId",
            "flag":"flag"
        ]
    }
    
}