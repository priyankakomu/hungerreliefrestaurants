//
//  MapViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Gaddam,Vishnu Tulasi on 3/15/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation
import MessageUI

//Class to find nearest Ngos using maps
class MapViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate ,Operation{
    
    @IBOutlet weak var mapView: MKMapView!//outlet to map
    let locationManager = CLLocationManager() // Creates a object to CLLocation Manager
    var myLocations: [CLLocation] = [] // Array of CLLocation
    var matchingItems: [MKMapItem] = [MKMapItem]()
    var res:Restaurant!//
    var kinveyClientOperation:KinveyOperation! // Creates reference to KinveyOperation
    var ngosLocations:[Ngos] = [] // creates an Ngo array
    var closestLocation:CLLocation! // create a varaible of type CLCLocation
    let defaults = NSUserDefaults.standardUserDefaults()
    //Action method to find Nearest Location
    func getLocations(sender: AnyObject) {
        locationInLocations(ngosLocations, closestToLocation: closestLocation)
    }
    //function to find nearest Locations to current locations
    func locationInLocations(locations: [Ngos], closestToLocation location: CLLocation) -> CLLocation? {
        let smallestDistance: CLLocationDistance = 200000
        var closLocs:[Ngos] = []
        for loc in locations {
            let distance = location.distanceFromLocation(CLLocation(latitude: loc.latitude, longitude: loc.longitude))
            if  distance == 0 || distance < smallestDistance {
                closestLocation = CLLocation(latitude: loc.latitude, longitude: loc.longitude)
                closLocs.append(loc)
            }
        }
        let  mapView1:MKMapView
        for cl in closLocs {
            var here1:MKPointAnnotation = MKPointAnnotation()
            here1.title = cl.ngosName
            here1.subtitle = cl.phoneNumber
            let lat = cl.latitude
            let long = cl.longitude
            here1.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            mapView.addAnnotation(here1)
            mapView.setCenterCoordinate(here1.coordinate, animated: true)
        }
        return closestLocation
    }
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!)
        -> MKAnnotationView! {
            var pinView:MKPinAnnotationView! = mapView.dequeueReusableAnnotationViewWithIdentifier("pin") as!
                MKPinAnnotationView!
            if pinView == nil {
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
                pinView.pinTintColor = UIColor.redColor()
                pinView.canShowCallout = true
                let callBTN = UIButton(type: UIButtonType.DetailDisclosure)
                pinView.rightCalloutAccessoryView = callBTN
                callBTN.addTarget(self, action: Selector("showMoreInfo:"),
                    forControlEvents: UIControlEvents.TouchUpInside)
            }
            return pinView
    }
    func mapView(mapView: MKMapView!, annotationView: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if(annotationView.tag == 0){
            return
        }
        if control == annotationView.rightCalloutAccessoryView {
            let message =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("message") as! SendSMSViewController//// intantiates a SendSMSviewController by clicking on that pin
            message.phoneNumber = (annotationView.annotation?.subtitle)! //sets the PhoneNumber as subtitle
            self.navigationController?.pushViewController(message, animated: true)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        kinveyClientOperation = KinveyOperation(operation:self) // Initializes Kinvey operation class
        kinveyClientOperation.fetchCoordinates(userValue) // Calls fetchCoordinates in Kinvey operation Class
        kinveyClientOperation.fetchNgo() // calls fetchNgo in kinvey operation Class
        self.navigationItem.title = "Nearby Locations" //  Sets title for navigation item
        let logout: UIBarButtonItem = UIBarButtonItem(title: "Get Locations", style: UIBarButtonItemStyle.Plain, target: self, action:Selector("getLocations:")) // Creates a bar button item of name Get Locations
        self.navigationItem.rightBarButtonItem = logout
    }
    //This Function Fetches the Restaurant and append to myLocation and set region of that location in the map
    func fetchLocation(restaurant:Restaurant){
        myLocations.append(CLLocation(latitude: restaurant.lat, longitude: restaurant.long))
        closestLocation = myLocations[0]
        let latitude:CLLocationDegrees = restaurant.lat
        let longitude:CLLocationDegrees = restaurant.long
        let location = CLLocationCoordinate2D(latitude: latitude , longitude: longitude)
        let span = MKCoordinateSpanMake(0.0005, 0.0005)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        self.mapView.delegate = self
    }
    //This function Fetches the ngos into NgosLocations
    func fetchNgoRecord(ngo:[Ngos]){
        ngosLocations = ngo
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

