//
//  LoginViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/13/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit
//View controller to authenticate the restaurants
class LoginViewController: UIViewController,Operation{
    
    @IBOutlet weak var emailIdTF: UITextField!
    //Textfield to store emailid
    @IBOutlet weak var passwordTF: UITextField! //Textfield to store password
    var kinveyClientOperation:KinveyOperation! //Reference to the KinveyOperation class
    var  restaurantlogin:RestaurantLogin! //Reference to the RestaurantLogin class
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyClientOperation = KinveyOperation(operation:self)
        self.navigationItem.title = "Login" //Sets the title for navigation item
    }
    //Function to authenticate the login of the user
   
    @IBAction func login(sender: AnyObject) {
    
        if (emailIdTF.text!.isEmpty || passwordTF.text!.isEmpty){
            self.displayAlertControllerWithTitle("Login Failed", message: "All fields are Required")
        }
        else{
            restaurantlogin = RestaurantLogin(emailId: emailIdTF.text!, password: passwordTF.text!)
            kinveyClientOperation.login(restaurantlogin)
        }
    }
    //Function to display alert on login fail
    func loginFailed() {
        self.displayAlertControllerWithTitle("Login failed", message:"TryAgain")
    }
    //Function to display alert on login success
    func loginSuccess(){
        self.displayAlertControllerWithSuccess("Login Successful", message:"Welcome!")
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.title = "Login"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //Function to display alert messages
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    //Function to display alert messages and navigate to ListViewController if the login is successful
    func displayAlertControllerWithSuccess(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("loginSuccess", sender: self)}))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
}
