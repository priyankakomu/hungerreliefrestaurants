//
//  ListViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 4/1/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit
//Class to list the donated food items in a table
class ListViewController: UIViewController,UIApplicationDelegate,UITableViewDataSource,Operation {
    
    var kinveyClientOperation:KinveyOperation! // Reference to the KinveyOperation class
    var itemArray:[Item] = [] //Variable to store the items in an array
    @IBOutlet weak var tableView: UITableView! //outlet to the table view
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "View Donated Items" //Sets the title for navigation item
        self.navigationItem.backBarButtonItem=nil //hides the backbar button in navigation item
        kinveyClientOperation = KinveyOperation(operation:self) // Initializes KinveyOperation Class
        let logout: UIBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action:Selector("logout1:")) //Creates a barbutton item named logout
        let nearBy: UIBarButtonItem = UIBarButtonItem(title: "Nearby", style: UIBarButtonItemStyle.Plain, target: self, action:Selector("map:")) //Creates a barbutton item named nearBy
        self.navigationItem.setRightBarButtonItems([logout,nearBy], animated: true)
        let addItem: UIBarButtonItem = UIBarButtonItem(title: "Add Item", style: UIBarButtonItemStyle.Plain, target: self, action: "Add") //Creates barbutton item for addItem
        self.navigationItem.leftBarButtonItem = addItem
    }
    override func viewWillAppear(animated: Bool) {
        kinveyClientOperation.fetchItem() //Calls fetchItem to  Fetch the records from itemStore
        self.tableView.reloadData()
    }
    //Function to perform logout operations and navigate to LoginViewController
    func logout1(Any:AnyObject){
        if KCSUser.activeUser() != nil {
            KCSUser.activeUser().logout()
            let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
            self.navigationController?.pushViewController(login, animated: true)
        }
    }
    //Function to navigate to MapViewController
    func map(Any:AnyObject){
        let map3 =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("map") as! MapViewController
        self.navigationController?.pushViewController(map3, animated: true)
    }
    //Function to navigate to ItemsViewController
    func Add(){
        let add =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("Add") as! AddItemViewController
        self.navigationController?.pushViewController(add, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    //Function to fetch the items from itemStore into items array
    func fetch(item: [Item]) {
        itemArray = item
        self.tableView.reloadData()
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tablelist", forIndexPath: indexPath)
        let nameLbl : UILabel = cell.viewWithTag(11) as! UILabel
        let typeLbl : UILabel = cell.viewWithTag(12) as! UILabel
        let quantityLbl:UILabel = cell.viewWithTag(78) as! UILabel
        nameLbl.text = itemArray[indexPath.row].itemName
        typeLbl.text = itemArray[indexPath.row].type
        quantityLbl.text = itemArray[indexPath.row].quantity
        return cell
    }
}
