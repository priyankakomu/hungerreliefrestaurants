//
//  ItemsViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Gaddam,Vishnu Tulasi on 3/15/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit
//Class to add new food items to the donated list
class AddItemViewController: UIViewController,Operation {
    
    @IBOutlet weak var itemNameTF: UITextField! //variable to store itemName
   let defaults = NSUserDefaults.standardUserDefaults()
    @IBOutlet weak var typeTF: UITextField! //variable to store the type of food
    
    @IBOutlet weak var quantityTF: UITextField! //variable to store the quantity of food
    var kinveyClientOperation:KinveyOperation! // reference to the KinveyOperation class
    var item:Item! //variable item to create reference to class Item
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyClientOperation = KinveyOperation(operation:self) //Initializes the KinveyOpertation class
    }
    //Funciton to create Item object and adds it to the donated list
    @IBAction func AddItem(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        item = Item(itemName: itemNameTF.text!, type:typeTF.text! , quantity:quantityTF.text!,emailId:userValue )
        kinveyClientOperation.saveItem(item) //calls the  saveItem function in kinvey operation class to save the item to store
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
